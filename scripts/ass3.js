//Shlomo Bensimhon 1837702

/**
 * Fetches a quote from https://ron-swanson-quotes.herokuapp.com/v2/quotes
 * and displays the quote on the web page.
 *
 * @author: shlomy_b
 */

"use strict";
document.addEventListener("DOMContentLoaded", setup);

 /**
   * Adds a click event listener to the button with the id "fetchButton", once
   * the button is clicked, the method fetcher is called. 
   *
   */
function setup(){
    document.querySelector("#fetchButton").addEventListener("click", fetcher);
}

 /**
   * The fetcher method fetches an api from https://ron-swanson-quotes.herokuapp.com/v2/quotes,
   * if response.ok is false, an error code is thrown, via the printError method. If the 
   * response.ok is true, the quote is sent to the createQuote method. 
   *
   */
function fetcher(){
    fetch('https://ron-swanson-quotes.herokuapp.com/v2/quotes')
    .then (response => {
        if(!response.ok){
            throw new Error('Status code: '  + response.status);
        }
            fetch('https://ron-swanson-quotes.herokuapp.com/v2/quotes')
            .then (response => response.json())
            .then (json => createQuote(json));
    })
    .catch(error => printError());
}

 /**
   * The createQuote method finds the paragraph where the quote is stored 
   * and adds or replaces texts inside of it.
   *
   * @param {obj} json The response containing the quote
   */
function createQuote(json){
    let newP = document.querySelector("#newQuote");
    newP.textContent = json.toString();
}

 /**
   * The printError prints an error code into the newQuote paragraph
   * and changes its id top modify the text.
   *
   */
function printError(){
    let newP = document.querySelector("#newQuote");
    newP.textContent =  "ERROR. PLEASE TRY AGAIN!";
    newP.setAttribute("id", "ErrorLine");
}